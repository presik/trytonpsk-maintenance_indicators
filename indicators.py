# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from decimal import Decimal
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateAction
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

EK = Decimal('2.303')


class Equipment(metaclass=PoolMeta):
    __name__ = 'maintenance.equipment'
    reliability = fields.Function(fields.Numeric('Reliability',
            digits=(16, 2)), 'get_reliability')
    availability = fields.Function(fields.Numeric('Availability',
            digits=(16, 2)), 'get_availability')
    maintainability = fields.Function(fields.Numeric('Maintainability',
            digits=(16, 2)), 'get_maintainability')
    sum_hour_repair = fields.Function(fields.Numeric('Sum Hour Repair',
            digits=(16, 2)), 'get_sum_hour_repair')
    intervention_cost = fields.Function(fields.Numeric('Intervention Cost',
            digits=(16, 2)), 'get_intervention_cost')
    percentage_corrective = fields.Function(fields.Numeric('Percentage Corrective',
            digits=(16, 2)), 'get_percentage_corrective')
    percentage_preventive = fields.Function(fields.Numeric('Percentage Preventive',
            digits=(16, 2)), 'get_percentage_preventive')

    @classmethod
    def __setup__(cls):
        super(Equipment, cls).__setup__()

    def get_reliability(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        time_delta = self._compute_date(start_date, end_date, 'Hour')
        res = self._reliability(start_date, end_date, time_delta)
        return res

    def get_availability(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        time_delta = self._compute_date(start_date, end_date, 'Hour')
        res = self._availability(start_date, end_date, time_delta, 'Hour')
        return res

    def get_maintainability(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        res = self._maintainability(start_date, end_date)
        return res

    def get_sum_hour_repair(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        dom = ['OR',[('type_activity.activity', '=', 'corrective')],
                    [('type_activity.activity', '=', 'preventive')],
                ]
        requests = self._request_services(start_date, end_date, dom)
        mnt_vars = self._compute_maintenance_time(requests, uom='Hour')
        return mnt_vars['sum_mnt_time']

    def get_intervention_cost(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        dom = ['OR',[('type_activity.activity', '=', 'corrective')],
                    [('type_activity.activity', '=', 'preventive')],
                ]
        requests = self._request_services(start_date, end_date, dom)
        sum_intervention_cost = Decimal(0)
        for req in requests:
            if hasattr(req, 'operation_cost'):
                sum_intervention_cost += req.operation_cost
        return sum_intervention_cost

    def get_percentage_corrective(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        dom_total = ['OR',[('type_activity.activity', '=', 'corrective')],
                    [('type_activity.activity', '=', 'preventive')],
                ]
        total_req = self._request_services(start_date, end_date, dom_total)

        dom_corrective = [('type_activity.activity', '=', 'corrective')]
        corrective_req = self._request_services(start_date, end_date, dom_corrective)
        percentage = None
        if total_req:
            percentage = (float(len(corrective_req)) / len(total_req)) * 100
        return percentage

    def get_percentage_preventive(self, name):
        start_date =  Transaction().context.get('start_date')
        end_date = Transaction().context.get('end_date')
        dom_total = ['OR',[('type_activity.activity', '=', 'corrective')],
                    [('type_activity.activity', '=', 'preventive')],
                ]
        total_req = self._request_services(start_date, end_date, dom_total)

        dom_preventive = [('type_activity.activity', '=', 'preventive')]
        preventive_req = self._request_services(start_date, end_date, dom_preventive)
        percentage = None
        if total_req:
            percentage = (float(len(preventive_req)) / len(total_req)) * 100
        return percentage

    def _reliability(self, start_date, end_date, time_delta):
        # Reliability Index = (Rango de Tiempo - Sumatoria de Horas en Reparación) / Número de Fallas en ese tiempo.
        dom = ('type_activity.activity', '=', 'corrective')
        requests = self._request_services(start_date, end_date, dom)
        res = self._compute_maintenance_time(requests)
        if not requests:
            return None
        fails_number = len(requests)
        return float(time_delta - res['sum_rfh']) / fails_number

    def _availability(self, start_date, end_date, time_delta, uom):
        dom = ['OR',[('type_activity.activity', '=', 'corrective')],
                    [('type_activity.activity', '=', 'preventive')],
                ]
        requests = self._request_services(start_date, end_date, dom)
        if not requests:
            return None
        mnt_vars = self._compute_maintenance_time(requests, uom)
        return (time_delta - mnt_vars['sum_mnt_time']) / time_delta

    def _maintainability(self, start_date, end_date):
        dom = ('type_activity.activity', '=', 'corrective')
        requests = self._request_services(start_date, end_date, dom)
        if not requests:
            return None
        mnt_vars = self._compute_maintenance_time(requests)
        return  float(mnt_vars['sum_rfh']) / mnt_vars['num_repairs']

    def _request_services(self, start_date, end_date, domainx):
        Request = Pool().get('maintenance.request_service')
        requests = Request.search([
                ('equipment.id', '=', self.id),
                domainx,
                ('create_date', '>=', start_date),
                ('create_date', '<=', end_date),
                ])
        return requests

    def _compute_maintenance_time(self, requests, uom='Day'):
        res = {}
        sum_mnt_time = Decimal(0)  # Sum Maintenance Time
        sum_rfh = Decimal(0) # Sum Repair Forecast Hours
        for req in requests:
            sum_mnt_time += self._compute_date(req.request_date,
                    req.effective_date, uom)
            if req.repair_time:
                sum_rfh += req.repair_time

        res['num_repairs'] = len(requests)
        res['sum_mnt_time'] = sum_mnt_time
        res['sum_rfh'] = sum_rfh
        # Sum Repair Forecast Days
        print(float(sum_rfh)/24)
        res['sum_rfd'] = (float(sum_rfh)/24)
        return res

    def _compute_date(self, start_date, end_date, uom):
        time_delta = self.compute_uom_timedelta(
                    start_date, end_date, uom)
        if not time_delta:
            time_delta = Decimal(0)
        return time_delta

    def compute_uom_timedelta(self, start_date, end_date, uom):
        if not start_date or not end_date:
            return None
        dtime = end_date - start_date
        dtime = dtime.total_seconds()
        if uom == 'Day':
            timedelta = (dtime/3600)/24
        elif uom == 'Hour':
            timedelta = dtime/3600
        elif uom == 'Second':
            timedelta = dtime
        else:
            timedelta = 0
        timedelta = Decimal(str(round(timedelta, 2)))
        return timedelta


class IndicatorsByEquipmentStart(ModelView):
    'Indicators by Equipment Start'
    __name__ = 'maintenance.by_equipment.start'
    start_date = fields.DateTime('Start Date')
    end_date = fields.DateTime('End Date')

    @staticmethod
    def default_end_date():
        return datetime.datetime.now()


class IndicatorsByEquipment(Wizard):
    'Indicators by Equipment'
    __name__ = 'maintenance.by_equipment'
    start = StateView('maintenance.by_equipment.start',
        'maintenance_indicators.indicators_by_equipment_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Open', 'open', 'tryton-ok', default=True),
            ])
    open = StateAction('maintenance_indicators.act_indicators_equipment_tree')

    def do_open(self, action):
        context = {}
        Request = Pool().get('maintenance.request_service')
        requests = Request.search([])
        active_equips = list(set([req.equipment.id for req in requests]))
        context['active_ids'] = active_equips

        if self.start.start_date:
            context['start_date'] = self.start.start_date
        else:
            context['start_date'] = datetime.date.max
        if self.start.end_date:
            context['end_date'] = self.start.end_date
        else:
            context['end_date'] = datetime.date.max
        action['pyson_context'] = PYSONEncoder().encode(context)
        return action, {}
